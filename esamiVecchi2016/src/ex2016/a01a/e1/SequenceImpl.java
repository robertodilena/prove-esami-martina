package ex2016.a01a.e1;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SequenceImpl<X> implements Sequence<X> {

	private final List<X> seq;

	public SequenceImpl(final List<X> from) {
		this.seq = Collections.unmodifiableList(from);
	}

	@Override
	public Optional<X> getAtPosition(final int position) {
		if (position >= this.seq.size() || position < 0) {
			return Optional.empty();
		} else {
			return Optional.of(this.seq.get(position));
		}
	}

	@Override
	public int size() {
		return this.seq.size();
	}

	@Override
	public List<X> asList() {
		return this.seq;
	}

	@Override
	public void executeOnAllElements(Executor<X> executor) {
		this.seq.forEach(e -> executor.execute(e));
	}
	
}
