package ex2016.a01b.e1_2;

import java.util.List;

public class ListBuilderWithSize<X> implements ListBuilder<X> {

	private final ListBuilder<X> builder;
	private int size;
	
	public ListBuilderWithSize(final ListBuilder<X> builder, final int size) {
		this.size = size;
		this.builder = builder;
	}
	
	@Override
	public void addElement(X x) {
		if (size != 0) {
			builder.addElement(x);
			size--;
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public List<X> build() {
		if (size == 0) {
			return builder.build();
		} else {
			throw new IllegalStateException();
		}
		
	}

}
