package ex2016.a01b.e1_2;

import java.util.Collection;

public class BuildersImpl implements Builders {

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
		return new BasicListBuilder<>();
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {
		return new ListBuilderWithSize<>(new BasicListBuilder<>(),size);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
		return new ListBuilderOf<>(new BasicListBuilder<>(), from);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {
		return new ListBuilderOf<>(this.makeBuilderWithSize(size), from);
	}

}
