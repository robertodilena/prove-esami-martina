package ex2016.a03b.e1;

public class TimeImpl implements Time{
	
	private final int ore;
	private final int minuti;
	private final int secondi;

	public TimeImpl(final int ore, final int minuti, final int secondi) {
		this.ore = ore;
		this.minuti = minuti;
		this.secondi = secondi;
	}
	
	@Override
	public int getHours() {
		return this.ore;
	}

	@Override
	public int getMinutes() {
		return this.minuti;
	}

	@Override
	public int getSeconds() {
		return this.secondi;
	}

	@Override
	public String getLabel24() {
		return (format(ore) + ":" + format(minuti) + ":" + format(secondi));
	}

	private String format (int orario) {
		return (orario<10 ? "0" + orario : String.valueOf(orario));
	}
	@Override
	public int getSecondsFromMidnight() {
		return (ore*60*60+minuti*60+secondi);
	}

}
