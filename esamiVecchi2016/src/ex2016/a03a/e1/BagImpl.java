package ex2016.a03a.e1;

import java.util.ArrayList;

import java.util.List;
import java.util.Set;

public class BagImpl<X> implements Bag<X> {

	private final List<X> list ;
	
	public BagImpl(final List<X> from) {
		list = new ArrayList<>(from);
		System.out.println(list);
	}
	
	public BagImpl(final Set<X> from) {
		list = new ArrayList<>(from);
		System.out.println(list);
	}
	
	@Override
	public int numberOfCopies(X x) {
		int counter=0;
		for (X el : list) {
			if (el.equals(x)) {
				counter++;
			}
		}
		return counter;
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public List<X> toList() {
		return this.list;
	}

}
