package ex2016.a02b.e1;

public class DeviceFactoryImpl implements DeviceFactory {

	@Override
	public Device createDeviceNeverFailing() {
		return new DeviceImpl();
	}

	@Override
	public Device createDeviceFailingAtSwitchOn(final int count) {
		return new FailingDevice(new DeviceImpl(), count);
	}

	@Override
	public LuminousDevice createLuminousDeviceFailingAtMaxIntensity(int maxIntensity) {
		// TODO Auto-generated method stub
		return new LuminousDeviceImpl(maxIntensity);
	}

}
