package ex2016.a02b.e1;

public class LuminousDeviceImpl implements LuminousDevice {

	private final int maxLevel;
	private int luminousLevel = 0;

	private boolean isOn;
	private boolean isWorking = true;
	
	public LuminousDeviceImpl(final int max) {
		maxLevel = max;
		isOn = false;
	}
	
	@Override
	public void on() {
		if (luminousLevel != maxLevel && isWorking != false) {
			isOn = true;
		} else {
			isOn = false;
			isWorking = false;
		}
	}

	@Override
	public void off() {
		isOn = false;

	}

	@Override
	public boolean isOn() {
		return isOn;
	}

	@Override
	public boolean isWorking() {
		/*if (luminousLevel != maxLevel || isWorking != false) {
			return true;
		} else {
			return false;
		}*/
		return isWorking;
	}

	@Override
	public void dim() {
		if (luminousLevel > 0 ) {
			luminousLevel--;
		}

	}

	@Override
	public void brighten() {
		
		
		if (luminousLevel < maxLevel) {
			luminousLevel++;
			if (luminousLevel == maxLevel && isOn == true){
				isOn = false;
				isWorking = false;
			}
		}

	}

	@Override
	public int getIntesity() {
		return luminousLevel;
	}

}
