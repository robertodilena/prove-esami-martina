package ex2016.a02b.e1;

public class DeviceImpl implements Device {

	private boolean isOn = false;
	
	public DeviceImpl() {
	
	}
	
	@Override
	public void on() {
		isOn = true;
	}

	@Override
	public void off() {
		isOn = false;
	}

	@Override
	public boolean isOn() {
		return isOn;
	}

	@Override
	public boolean isWorking() {
		return true;
	}

}
