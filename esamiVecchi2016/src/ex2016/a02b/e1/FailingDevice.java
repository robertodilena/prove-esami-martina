package ex2016.a02b.e1;

public class FailingDevice implements Device {
	
	private final Device basic;
	private final int maxSwitchOn;
	private int numberSwitchOn = 0;
	
	public FailingDevice(final Device device, final int max) {
		basic = device;
		maxSwitchOn=max;		
	}
	
	@Override
	public void on() {
		if (basic.isWorking() && numberSwitchOn < maxSwitchOn) {
			numberSwitchOn++;
			basic.on();
		}
	}

	@Override
	public void off() {
		basic.off();
	}

	@Override
	public boolean isOn() {
		return (basic.isOn() && numberSwitchOn<maxSwitchOn)? true:false;
	}

	@Override
	public boolean isWorking() {
		if (basic.isWorking() && numberSwitchOn < maxSwitchOn) {
			return true;
		} else {
			return false;
		}
	}

}
