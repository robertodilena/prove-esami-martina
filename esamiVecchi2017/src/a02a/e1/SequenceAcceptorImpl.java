package a02a.e1;

public class SequenceAcceptorImpl implements SequenceAcceptor {

	private BasicSequence basic;
	
	public SequenceAcceptorImpl() {
	}

	@Override
	public void reset(Sequence sequence) {
		switch (sequence) {
		case POWER2: 
			basic = new Power2Sequence();
		case RAMBLE:
			basic = new RambleSequence();
		case FLIP:
			basic = new FlipSequence();
		case FIBONACCI:
			basic = new FibonacciSequence();
		}
	}

	@Override
	public void reset() {
		basic.reset();
	}

	@Override
	public void acceptElement(int i) {
		basic.acceptElement(i);
	}

	

}
