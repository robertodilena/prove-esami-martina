package a02a.e1;

public class FlipSequence extends BasicSequence{

	@Override
	public void reset() {
		actual = 1;
		next = 0;
	}

	@Override
	public void acceptElement(int i) {
		if (actual == i) {
			final int temp = next;
			next = actual;
			actual = temp;
		} else {
			throw new IllegalStateException();
		}
	}

}
