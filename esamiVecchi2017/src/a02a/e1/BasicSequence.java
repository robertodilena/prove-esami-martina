package a02a.e1;

public abstract class BasicSequence implements SequenceAcceptor {

	protected int actual;
	protected int next;

	@Override
	public abstract void reset() ;

	@Override
	public abstract void acceptElement(int i);

	@Override
	public void reset(Sequence sequence) {
		this.reset();
	}
	
	

}
