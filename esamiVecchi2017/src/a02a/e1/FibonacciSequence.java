package a02a.e1;

public class FibonacciSequence extends BasicSequence {

	@Override
	public void acceptElement(int i) {
		if (i == actual) {
			final int temp = next;
			next = next + actual;
			actual = temp;
		} else {
			throw new IllegalStateException();
		}
	}

	@Override
	public void reset() {
		actual = 1;
		next=1;
	}

}
