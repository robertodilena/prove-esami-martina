package a01b.e1;

import java.util.function.Supplier;
import java.util.stream.IntStream;

public class TraceFactoryImpl implements TraceFactory{

	public TraceFactoryImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		// TODO Auto-generated method stub
		return new TraceImpl <X>(IntStream.iterate(0, i -> i+sdeltaTime.get()).
					limit(size).
					<Event<X>>mapToObj(i -> new EventImpl<>(i, svalue.get())).
					iterator());
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		return fromSuppliers(sdeltaTime, ()-> value, size);
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return fromSuppliers(()->1, svalue, size);
	}

}
