package a01b.e2;

import java.util.List;

public interface Model {

	boolean hit(int index);
	
	List<Integer> values ();
	
}
