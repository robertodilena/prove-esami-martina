package a01b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class ModelImpl implements Model {

	private final List<Integer> list;
	private final List<Integer> remaining;
	private final Random seed = new Random();
	private final static int DROPPED = 100;
	
	public ModelImpl(final int size) {
		list = new ArrayList<>();
		IntStream.range(0, size).forEach(el -> list.add(seed.nextInt(100)));
		remaining = new ArrayList<>(list);
	}
	
	@Override
	public boolean hit(int index) {
		final int elem = list.get(index);
		for (int el: remaining) {
			if (el < elem) {
				return false;
			}
		}
		remaining.set(index, DROPPED);
		return true;
	}

	@Override
	public List<Integer> values() {
		return list;
	}

	
}
