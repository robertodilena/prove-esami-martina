package a01b.e2;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{

	private static final long serialVersionUID = -2300342035580724565L;
	private final List <JButton> buttons = new ArrayList<>();
	private final Model model;
	
	public GUI(int size){
		model = new ModelImpl(size);
		IntStream.range(0, size).forEach(i -> {
			buttons.add(new JButton("*"));
		});
		
		buttons.forEach(el-> {
			final int index = buttons.indexOf(el);
			buttons.get(index).addActionListener(e -> {
				if (model.hit(index)){
					final List<Integer> val = model.values();
					buttons.get(index).setEnabled(false);
					buttons.get(index).setText(val.get(index).toString());
				}
			});
			this.getContentPane().add(buttons.get(index));
		});
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		this.setVisible(true);
	}
	
}
