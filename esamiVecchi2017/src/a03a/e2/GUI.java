package a03a.e2;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame{

	private static final long serialVersionUID = 1L;
	private final List<JButton> buttons;
	private final Model model;

	public GUI(String fileName) {
		final JPanel outer = new JPanel(new BorderLayout());
		final JPanel main = new JPanel();
		final JPanel bottom = new JPanel();
		final JButton close = new JButton("Close");
		
		buttons = new ArrayList<>();
		model = new ModelImpl(fileName);
		
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		IntStream.range(0, model.getLinesNumber()).forEach(i -> {
			buttons.add(new JButton (model.getLineValue(i)));
			buttons.get(i).addActionListener(e -> {
				buttons.get(i).setEnabled(false);
				model.disable(i);
			});
			main.add(buttons.get(i));
		});
		
		close.addActionListener( e ->{
			model.append();
			System.exit(0);
		});
		bottom.add(close);
		outer.add(main,BorderLayout.CENTER);	
		outer.add(bottom,BorderLayout.SOUTH); // nell'esame il pulsante Close va messo però in basso, e possibilmente delle giuste dimensioni
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().add(outer);
		this.setSize(600,300);
		this.setVisible(true);
	}

}
