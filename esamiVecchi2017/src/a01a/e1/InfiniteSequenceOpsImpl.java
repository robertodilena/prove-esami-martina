package a01a.e1;

import java.util.Iterator;
import java.util.List;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps{
	
	public InfiniteSequenceOpsImpl() {
	}
	
	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return new InfiniteSequence<X>() {

			@Override
			public X nextElement() {
				return x;
			}
		};
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
		// TODO Auto-generated method stub
		return new InfiniteSequence<X>() {
			private int index = 0;
			@Override
			public X nextElement() {
				return l.get((index++)%l.size());
			}
			
		};
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
		return new InfiniteSequence<Double>() {
			private Double sum = 0.0;
			@Override
			public Double nextElement() {
				sum = 0.0;
				for (int i = 0;i<intervalSize; i++) {
					sum+=iseq.nextElement();
				}

				return (sum/intervalSize);
			}
		};
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return new InfiniteSequence<X>() {
			private int index;
			@Override
			public X nextElement() {
				for (index = 1; index < intervalSize; index++) {
					iseq.nextElement();
				}
				return iseq.nextElement();
			}
		};
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		return new InfiniteSequence<Boolean>() {
			
			@Override
			public Boolean nextElement() {
				return iseq.nextElement().equals(iseq.nextElement());
			}
		};
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		return new InfiniteSequence<Boolean>() {
			
			@Override
			public Boolean nextElement() {
				return isx.nextElement().equals(isy.nextElement());
			}
		};
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return new Iterator<X>() {

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public X next() {
				return iseq.nextElement();
			}
		};
	}

}
