package a01a.e2;

import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model {

	private final List<Integer> list;
	
	public ModelImpl(final int size) {
		list = new ArrayList<>();
		
		for (int i =0;i<size;i++) {
			list.add(0);
		}
	}
	
	@Override
	public String getValueOf(int index) {
		int temp = list.get(index);
		temp ++;
		list.remove(index);
		list.add(index, temp);
		return list.get(index).toString();
	}

	@Override
	public List<Integer> getValues() {
		return list;
	}

	@Override
	public boolean isEnabled(int index) {
		return ! (list.get(index) == list.size());
	}

}
