package a02b.e2;

import java.util.List;

public interface Model {
	String removeLine (int index);
	String concatLine (int index);
	String addLine (int index);
	List<String> lines();
}
