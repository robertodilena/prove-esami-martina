package a02b.e2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model {

	private final File file;
	private final List<String> lines;
	
	public ModelImpl(String fileName) {
		file = new File (fileName);
		lines = new ArrayList<>();
		
		loadStrings();
	}

	@Override
	public String removeLine(int index) {
		final String removed = (lines.remove(index));
		
		updateFile();
		
		return removed;
	}

	@Override
	public String concatLine(int index) {
		final String s = lines.get(index) + lines.get(index);
		lines.set(index, s);
		
		updateFile();
		
		return s;
	}

	@Override
	public String addLine(int index) {
		final String s = "*" + lines.get(index);
		lines.add(index+1, s);
		
		updateFile();
		
		return s;
	}

	@Override
	public List<String> lines() {
		return lines;
	}

	private void loadStrings() {
		lines.clear();
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
			reader.lines().forEach(line -> {
				lines.add(line);
			});
		} catch (IOException e) {
			System.out.println("IOException.");
			e.printStackTrace();
		}
	}
	
	private void updateFile() {
		try{
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write("");/*svuoto il file*/
			writer.close();
			writer = new BufferedWriter(new FileWriter(file));
			for (int i = 0; i<lines.size();i++) {
				writer.write(lines.get(i));
			}
			writer.close();
		} catch (IOException e) {
			System.out.println("IOException.");
			e.printStackTrace();
		}
	}
}
