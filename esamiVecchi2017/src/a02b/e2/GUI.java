package a02b.e2;

import java.awt.FlowLayout;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import javax.swing.*;

public class GUI {
	
	private final Model model;
	
	final JComboBox<String> combo;

	public GUI(String fileName) throws IOException {
		final JFrame gui = new JFrame();
		final JPanel main = new JPanel();
		final JButton remove = new JButton("Remove");
		final JButton add = new JButton("Add");
		final JButton concat = new JButton("Concat");
		model = new ModelImpl(fileName);
		combo = new JComboBox<>();
		
		updateComboBox();
		
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		remove.addActionListener(e -> {
			System.out.println(model.removeLine(combo.getSelectedIndex()));
			updateComboBox();
		});	
		
		concat.addActionListener(e ->{
			System.out.println(model.concatLine(combo.getSelectedIndex()));
			updateComboBox();
		});
		
		add.addActionListener(e ->{
			System.out.println(model.addLine(combo.getSelectedIndex()));
			updateComboBox();
		});
		
		main.setLayout(new FlowLayout());
		main.add(combo);
		main.add(remove);
		main.add(add);
		main.add(concat);
		gui.getContentPane().add(main);
		gui.pack();
		gui.setVisible(true);
	}
	
	private void updateComboBox() {
		final List<String> list = new ArrayList<>(model.lines());
		combo.removeAllItems();
		IntStream.range(0, list.size()).forEach(e -> {
			combo.addItem(list.get(e));
		});
		combo.setSelectedIndex(0);
	}

}
