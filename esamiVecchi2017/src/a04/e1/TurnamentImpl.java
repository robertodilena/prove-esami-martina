package a04.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class TurnamentImpl implements Turnament {

	private final List<Player> playersStarting;
	private final List<Player> players;
	private final List<Match> matches;
	private int counter=0;
	private boolean start = false;
	
	public TurnamentImpl() {
		players = new ArrayList<>();
		matches = new ArrayList<>();
		playersStarting = new ArrayList<>();
	}
	
	@Override
	public Player makePlayer(int id, String name) {
		return new PlayerImpl(id, name);
	}

	@Override
	public Match makeMatch(Player p1, Player p2) {
		return new MatchImpl(p1, p2);
	}

	@Override
	public void registerPlayer(Player player) {
		players.add(player);
	}

	@Override
	public void startTurnament() {
		start = true;
		playersStarting.addAll(players);
	}

	@Override
	public List<Player> getPlayers() {
		return playersStarting;
	}

	@Override
	public List<Match> getPendingGames() {
		if (start) {
			if (matches.isEmpty()) {
				calculateMatch();
				players.clear();
				IntStream.range(0, matches.size()).forEach(i -> players.add(null));
			}
			return matches;
		}else {
			return Collections.emptyList();
		}
	}

	@Override
	public void playMatch(Match match, Player winner) {
		if (start) {
			if (matches.isEmpty()) {
				calculateMatch();
				players.clear();
				IntStream.range(0, matches.size()).forEach(i -> players.add(null));
			}
			for (int i=0; i<matches.size();i++){
				if (matches.get(i).equals(match)) {
					players.set(i,winner);
					counter++;
					if (counter == matches.size()) {
						matches.clear();
					}
					break;
				} 
			}
		} 
		//System.out.println("PLAYERS:" + players);
	}

	@Override
	public boolean isTurnamentOver() {
		return (matches.isEmpty() && players.size()==1);
	}

	@Override
	public Player winner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Player> opponents(Player player) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void calculateMatch() {
		matches.clear();
		final int size = players.size();
		
		for(int i = 0 ; (i+1)<size; i=i+2) {
			//System.out.println("match between " + players.get(i)+ " and " + players.get(i+1));
			matches.add(new MatchImpl(players.get(i), players.get(i+1)));
		}
	}

}
